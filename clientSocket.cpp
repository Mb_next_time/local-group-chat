#include "clientSocket.hpp"

ClientSocket::ClientSocket() { }

ClientSocket::ClientSocket(unsigned int port, char* ip) 
: m_port(port)
, m_ip(ip) 
{ 
	
}

int ClientSocket::connectWithServer()
{	
	struct sockaddr_in SockAddr;
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(m_port);
	SockAddr.sin_addr.s_addr = inet_addr(m_ip);
	
	// create TCP-socket
	m_sock = socket(AF_INET, SOCK_STREAM, 0);
	
	// conneting to server
	// check connetion on error, if return 0 that success no errors
	// else return -1 connection have errors
	if(!connect(m_sock, (struct sockaddr *) &SockAddr, sizeof(SockAddr)))
	{
		return 0;
	}
	else
	{	
		return -1; 
	}
}

bool ClientSocket::sendData(char* msg)
{	
	if(!strcmp(msg, "exit"))
	{	
		disconnectByServer();
		return false;
	}
	
	send(m_sock, msg, strlen(msg), 0);
	return true;
}
	
bool ClientSocket::recvData()
{	
	char msg[255];
	
	// recieve message from server in array msg
	int data = recv(m_sock, msg, 255, 0);
	msg[data] = '\0';
	// if count data > 0, that receive data
	// else send flag false			
	if(data > 0)
	{	
		std::cout << "Input message: " << msg << std::endl;
		return true;
	}
	
	return false;
}

void ClientSocket::disconnectByServer()
{	
	close(m_sock);
}

ClientSocket::~ClientSocket() { }
