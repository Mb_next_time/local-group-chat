#include "server.hpp"

Server::Server(unsigned int port, unsigned int totalConnection) 
: m_port(port)
, m_totalConnection(totalConnection)
, m_currentNumberConnection(0)
, m_slaveSockets(new int[m_totalConnection]) 
, isWorking(false)
{

}

int Server::init()
{	
	m_masterSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	struct sockaddr_in SockAddr;
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(m_port);
	SockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	// bind socket to all free local interfaces and listen input connecting
	if(bind(m_masterSocket, (struct sockaddr *)(&SockAddr), sizeof(SockAddr)) ||
		listen(m_masterSocket, 5))
	{
		return -1;
	}
	
	for(int i = 0; i < m_totalConnection; i++)
	{
		m_slaveSockets[i] = -1;
	}
	
	isWorking = true;
	return 0;
}

void Server::run()
{	
	// While isWorking = true, server working
	while(isWorking)
	{
		recvData();
		acceptNewConnection();
	}
}

Server::~Server() 
{ 
	delete m_slaveSockets; 
}

void Server::recvData()
{	
	// init pull file descriptors(pull FD)
	FD_ZERO(&set);
	// add fd masterSocket in pull FD
	FD_SET(m_masterSocket, &set);
		
	std::cout << "Count connetions: " << m_currentNumberConnection << std::endl;
	// add fd slaveSockets in pull FD
	for(int i = 0; i < m_totalConnection; i++)
	{	
		FD_SET(m_slaveSockets[i], &set);
	}
	
	// finding highest-numbered fd in slave sockets
	int maxNumberSlaveSocket = maxElement(m_slaveSockets, m_totalConnection);
	// compare fd master socket and highest-numbered fd slave sockets
	int maxNumberSocket = compare(m_masterSocket, maxNumberSlaveSocket);
	
	// wait changes in pull fd 
	select(maxNumberSocket + 1, &set, nullptr, nullptr, nullptr);

	for(int i = 0; i != m_totalConnection; i++)
	{	
		// if slave sockets exist in pull fd and connected to server that
		// read data from clients
		if(FD_ISSET(m_slaveSockets[i], &set) && m_slaveSockets[i] != -1)
		{	
			int sizeBuf = 1024;
			char buf[sizeBuf];
							
			int recvSize = recv(m_slaveSockets[i], buf, sizeBuf, 0);
			buf[recvSize] = '\0';
			std::cout << "recvSize: " << recvSize << std::endl;
			// if recvSize empty, that client disconnect by server and
			// close connecting, decrement current number connection
			// else send data all clients besides sender
			if(!recvSize) 
			{	
				close(m_slaveSockets[i]);
				m_slaveSockets[i] = -1;
				--m_currentNumberConnection;
			}
			else if (recvSize)
			{	
				std::cout << "History: " << buf << std::endl;
				for(int iSend = 0; iSend != m_totalConnection; iSend++)
				{	
					if(m_slaveSockets[iSend] != m_slaveSockets[i] && m_slaveSockets[iSend] != -1) 
					{	
						send(m_slaveSockets[iSend], buf, strlen(buf), 0);
					}
				}
			}
		}
	}
}

void Server::acceptNewConnection()
{	
	// if master_socket exist in pull fd
	// that accept new connetion on server
	if(FD_ISSET(m_masterSocket, &set))
	{	
		int slaveSocket = accept(m_masterSocket, nullptr, nullptr);
		
		// if number current connetion less total number connetion
		// that add array slave sockets and increment number current connetion,
		// else send message to client that server is full and close connetion
		if(m_currentNumberConnection < m_totalConnection)
		{	
			for(int i = 0; i < m_totalConnection; i++)
			{
				if(m_slaveSockets[i] == -1)
				{
					m_slaveSockets[i] = slaveSocket;
					++m_currentNumberConnection;
					break;
				}
			}
		}
		else
		{	
			int sizeBuf = 1024;
			char buf[sizeBuf] = "Server is full\n";
			send(slaveSocket, buf, strlen(buf), 0);
			close(slaveSocket);
		}
	}
}

int max_element(int *mas, unsigned int size)
{		
	int max = 0;
	
	for(int i = 0; i < size; i++)
	{
		if(mas[i] > max)
		{
			max = mas[i];
		}
	}
	
	return max;
}

int max(int value1, int value2)
{
	return (value1 < value2) ? value2 : value1;
}
