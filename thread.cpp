#include "thread.hpp"

Thread::Thread() { }

int Thread::start()          
{ 
	return pthread_create( &m_threadId, nullptr, thread_func, this); 
}

int Thread::wait()          
{ 	
	return pthread_join( m_threadId, nullptr ); 
}

Thread::~Thread() { }

void* Thread::thread_func(void* d)
{ 	
	(static_cast<Thread*>(d))->run(); 
	
	return nullptr; 
}
