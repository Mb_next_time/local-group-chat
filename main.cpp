#include "server.hpp"

int main(int argc, char** argv)
{	
	int portServ = atoi(argv[1]);
	std::cout << "portServ: " << portServ << std::endl;
	int totalConnecting = atoi(argv[2]);
	
	Server server(portServ, totalConnecting);
	
	// if error init server, that exit from programm
	try
	{	
		int erroCode = server.init();
		
		if(erroCode)
		{
			throw erroCode;
		}
	}
	catch(int)
	{
		std::cout << "Error init server" << std::endl;
		return 1;
	}
	
	server.run();
	
	return 0;
}
