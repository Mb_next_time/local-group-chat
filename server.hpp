#ifndef SERVER_SOCKET_H
#define SERVER_SOCKET_H

#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include "helperFunctions.hpp"

class Server
{
public:
	Server(unsigned int port, unsigned int totalConnection);
	int init();
	void run();
	void recvData();
	void acceptNewConnection();
	~Server();
private:
	unsigned int m_port;
	unsigned int m_totalConnection;
	unsigned int m_currentNumberConnection;
	int m_masterSocket;
	int *m_slaveSockets;
	fd_set set;
	bool isWorking;
};

#endif
