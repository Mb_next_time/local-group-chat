#ifndef CLIENT_THREAD_H
#define CLIENT_THREAD_H

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#include "thread.hpp"
#include "clientSocket.hpp"

class ClientThread : public Thread
{
public:
	ClientThread();
    ClientThread(ClientSocket client, int state);
    virtual void run();
    ~ClientThread();
protected:
    ClientSocket m_client;
    int m_state;
};

#endif
