#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

int compare(int value1, int value2);
int maxElement(int *array, unsigned int size);

#endif
