#ifndef CLIENT_SOCKET_H
#define CLIENT_SOCKET_H

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

class ClientSocket
{
public:
	ClientSocket();
	ClientSocket(unsigned int port, char ip[]);
	int connectWithServer();
	void disconnectByServer();
	bool recvData();
	bool sendData(char *msg);
	~ClientSocket();
private:
	int m_sock;
	unsigned int m_port;
	char* m_ip;
};

#endif
