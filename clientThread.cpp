#include "clientThread.hpp"

ClientThread::ClientThread() {}

ClientThread::ClientThread(ClientSocket client, int state) 
: m_client(client)
, m_state(state) 
{
	
}
 
// Method work in two state input and output
// if state 1, that send message to server
// if state 2, that recieve message from server

void ClientThread::run()
{		
	switch(m_state)
	{
		case 1:
		{
			while(true)
			{	
				char msg[255];
				std::cin.getline(msg, 255);
				if(!m_client.sendData(msg))
				{
					std::cout << "Try to enter to chat next time" << std::endl;
					return;
				}
			}
			break;
		}
		case 2:
		{
			while(true)
			{	
				if(!m_client.recvData())
				{	
					std::cout << "Try to enter to chat next time" << std::endl;
					return;
				}
			}
			break;
		}
		default:
			std::cout << "Enter correct state for working thread\n";
			break;
	}
}

ClientThread::~ClientThread() {}
