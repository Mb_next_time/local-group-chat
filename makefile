all:
	g++ -std=c++11 -o main main.cpp server.cpp helperFunctions.cpp
	g++ -std=c++11 -o client client.cpp clientSocket.cpp clientThread.cpp thread.cpp -lpthread

clean:
	rm -f client main
