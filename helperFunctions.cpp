#include "helperFunctions.hpp"

int compare(int value1, int value2)
{
	return (value1 < value2) ? value2 : value1;
}

int maxElement(int *array, unsigned int size)
{		
	int max = 0;
	
	for(int i = 0; i < size; i++)
	{	
		max = compare(array[i], max);
	}
	
	return max;
}
