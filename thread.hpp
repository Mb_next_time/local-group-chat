#ifndef THREAD_H
#define THREAD_H

#include <pthread.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

class Thread
{
public:
    Thread();
    virtual void run() = 0;
    int start();
    int wait();
    ~Thread();
protected:
    pthread_t m_threadId;
    static void* thread_func(void* d);
};

#endif
