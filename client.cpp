#include <iostream>
#include <string.h>
#include <stdlib.h>
#include "clientThread.hpp"

int main(int argc, char** argv)
{	
	int portServ = atoi(argv[1]);
	char ipServ[15];
	strcpy(ipServ, argv[2]);
	
	ClientSocket client(portServ, ipServ);
	
	// if error connecting with server, that exit from programm
	try
	{	
		int erroCode = client.connectWithServer();
		if(erroCode)
		{
			throw erroCode;
		}
	}
	catch(int)
	{	
		std::cout << "Error connect with server" << std::endl;
		return 1;
	}

	// create thread for sending message
	ClientThread out(client, 1);
	// create thread for receiving message
	ClientThread in(client, 2);

	// start work threads
	out.start();
	in.start();
	
	if(out.wait() && in.wait())
	{	
		return 0;
	}
		
	return 0;
}
